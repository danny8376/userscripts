// ==UserScript==
// @name        YouTube Disable Up Next
// @author      KonomiKitten
// @namespace   youtubedisableupnext
// @description Disable "Up Next" otherwise known as AutoPlay
// @include     http://www.youtube.com*
// @include     https://www.youtube.com*
// @version     1.3.4
// @updateURL   https://gitlab.com/konomikitten/userscripts/-/raw/main/youtube-disable-up-next.user.js
// @downloadURL https://gitlab.com/konomikitten/userscripts/-/raw/main/youtube-disable-up-next.user.js
// @homepageURL https://gitlab.com/konomikitten/userscripts
// @supportURL  https://gitlab.com/konomikitten/userscripts/-/issues
// @icon        https://s.ytimg.com/yts/img/favicon_32-vflOogEID.png
// @grant       none
// @run-at      document-end
// @noframes
// ==/UserScript==

(function() {
  console.log("This script has been retired, please remove it "
             +"and install YouTube Tweaks instead")
})();