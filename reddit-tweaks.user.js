// ==UserScript==
// @name        Reddit Tweaks
// @author      KonomiKitten
// @namespace   reddittweaks
// @description Various tweaks for Reddit
// @include     http://old.reddit.com/*
// @include     https://old.reddit.com/*
// @version     1.4.0
// @updateURL   https://gitlab.com/konomikitten/userscripts/-/raw/main/reddit-tweaks.user.js
// @downloadURL https://gitlab.com/konomikitten/userscripts/-/raw/main/reddit-tweaks.user.js
// @homepageURL https://gitlab.com/konomikitten/userscripts
// @supportURL  https://gitlab.com/konomikitten/userscripts/-/issues
// @icon        https://www.redditstatic.com/desktop2x/img/favicon/favicon-96x96.png
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_registerMenuCommand
// @grant       GM_notification
// @run-at      document-start
// @noframes
// ==/UserScript==

(function() {
  'use strict';

  function setValue(value){
    if (GM_getValue(value) === undefined) GM_setValue(value, false);
  }

  setValue("flair_colour");
  GM_registerMenuCommand("Toggle Flair Colour", function() {
    if (GM_getValue('flair_colour')) {
      GM_setValue('flair_colour', false);
      GM_notification("Flair Colour: Disabled");
    }
    else {
      GM_setValue('flair_colour', true);
      GM_notification("Flair Colour: Enabled");
    }
  });

  setValue("chat_icon");
  GM_registerMenuCommand("Toggle Chat Icon", function() {
    if (GM_getValue('chat_icon')) {
      GM_setValue('chat_icon', false);
      GM_notification("Chat Icon: Disabled");
    }
    else {
      GM_setValue('chat_icon', true);
      GM_notification("Chat Icon: Enabled");
    }
  });

  setValue("upvote_fix");
  GM_registerMenuCommand("Toggle Upvote Fix", function() {
    if (GM_getValue('upvote_fix')) {
      GM_setValue('upvote_fix', false);
      GM_notification("Upvote Fix: Disabled");
    }
    else {
      GM_setValue('upvote_fix', true);
      GM_notification("Upvote Fix: Enabled");
    }
  });


  new window.MutationObserver(function() {
    if (!(GM_getValue('flair_colour'))) {
      var node = document.getElementsByTagName('head')[0];

      var style = document.createElement('style')
      style.type = 'text/css';
      style.innerHTML = 'span.linkflairlabel, span.flairrichtext {' +
        'display: inline-block !important;' +
        'margin-right: .5em !important;' +
        'padding: 0 2px !important;' +
        'background: #f5f5f5 !important;' +
        'color: #555 !important;' +
        'border: 1px solid #ddd !important;' +
        'border-radius: 2px !important;' +
        'overflow: hidden; !important;' +
        'max-width: 16em !important;}';
      document.head.appendChild(style);
    }
    
    if (GM_getValue('upvote_fix')) {
      var node = document.getElementsByTagName('head')[0];

      var style = document.createElement('style')
      style.type = 'text/css';
      style.innerHTML = 'body > .content .link .midcol {' +
        'width: 6ex !important;}';
      document.head.appendChild(style);
    }

    if (node) {
      this.disconnect();
    }
  }).observe(document, {
    'childList': true,
    'subtree': true
  });


  function chat(node) {
    if (GM_getValue('chat_icon')) { return; }
    if (!node.matches('a#chat')) { return; }
    console.log(node)
    node.style.display = 'none';
    console.log(node.previousSibling)
    node.previousSibling.style.display = 'none';
  }

  new window.MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      mutation.addedNodes.forEach(function(node) {
        // Only process element nodes
        if (node.nodeType != 1) return;

        // Chat
        chat(node)
      });
    });
  }).observe(document, {
    childList: true,
    subtree: true,
    attributes: false,
    attributeOldValue: false,
    characterData: false,
    characterDataOldValue: false
  });
})();
