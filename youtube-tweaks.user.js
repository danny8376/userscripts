// ==UserScript==
// @name        YouTube Tweaks
// @author      KonomiKitten
// @namespace   youtubetweaks
// @description YouTube Tweaks
// @include     http://www.youtube.com*
// @include     https://www.youtube.com*
// @version     2.5
// @updateURL   https://gitlab.com/konomikitten/userscripts/-/raw/main/youtube-tweaks.user.js
// @downloadURL https://gitlab.com/konomikitten/userscripts/-/raw/main/youtube-tweaks.user.js
// @homepageURL https://gitlab.com/konomikitten/userscripts
// @supportURL  https://gitlab.com/konomikitten/userscripts/-/issues
// @icon        https://icons.duckduckgo.com/ip/youtube.com.ico
// @grant       GM_listValues
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_deleteValue
// @grant       GM_registerMenuCommand
// @grant       GM_notification
// @run-at      document-start
// ==/UserScript==

(function () {
  'use strict';

  // Storage values
  let storage_values = ["autoplay_enabled", "autoplay_visible", "mixes_visible", "played_visible", "console_log"];

  // Set default values
  for (let value of storage_values) {
    if (GM_getValue(value) != undefined) { continue; }
    GM_setValue(value, false)
  }

  // Remove any saved values that are not in storage_values
  for (let key of GM_listValues()) {
    if (storage_values.includes(key)) { continue; }
    GM_deleteValue(key)
  }

  GM_registerMenuCommand("Toggle Autoplay", function() {
    if (GM_getValue('autoplay_enabled')) {
      GM_setValue('autoplay_enabled', false);
      GM_notification("Autoplay: Disabled");
    }
    else {
      GM_setValue('autoplay_enabled', true);
      GM_notification("Autoplay: Enabled");
    }
  });

  GM_registerMenuCommand("Toggle Autoplay Display", function() {
    if (GM_getValue('autoplay_visible')) {
      GM_setValue('autoplay_visible', false);
      GM_notification("Autoplay: Hidden");
    }
    else {
      GM_setValue('autoplay_visible', true);
      GM_notification("Autoplay: Visble");
    }
  });

  GM_registerMenuCommand("Toggle Mixes", function() {
    if (GM_getValue('mixes_visible')) {
      GM_setValue('mixes_visible', false);
      GM_notification("Mixes: Disabled");
    }
    else {
      GM_setValue('mixes_visible', true);
      GM_notification("Mixes: Enabled");
    }
  });

  GM_registerMenuCommand("Toggle Played", function() {
    if (GM_getValue('played_visible')) {
      GM_setValue('played_visible', false);
      GM_notification("Played: Disabled");
    }
    else {
      GM_setValue('played_visible', true);
      GM_notification("Played: Enabled");
    }
  });

  function autoplay(node) {
    if (GM_getValue('autoplay')) { return; }
    if (!node.matches('button.ytp-button[data-tooltip-target-id="ytp-autonav-toggle-button"][title="Autoplay is on"]')) { return; }
    (function pollClick() {
      if (node.matches('button.ytp-button[data-tooltip-target-id="ytp-autonav-toggle-button"][title="Autoplay is on"]')) {
        node.click();
        setTimeout(pollClick, 200);
      }
    })();
  }

  function autoplay_visible(node) {
    if (GM_getValue('autoplay_visible')) { return; }
    if (!node.matches('button.ytp-button[data-tooltip-target-id="ytp-autonav-toggle-button"]')) { return; }
    let style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = 'button.ytp-button[data-tooltip-target-id="ytp-autonav-toggle-button"] {display: none !important;}';
    document.head.appendChild(style);
  }

  function mixes(node) {
    if (GM_getValue('mixes_visible')) { return; }
    if (!node.matches('yt-icon.style-scope.ytd-thumbnail-overlay-bottom-panel-renderer')) { return; }
    var selectors = [
      'ytd-rich-item-renderer.style-scope.ytd-rich-grid-row', // Vidoes that appear on the main page
      'ytd-rich-item-renderer.style-scope.ytd-rich-grid-renderer', // Vidoes that appear on the main page
      'ytd-radio-renderer.style-scope.ytd-item-section-renderer', // Videos that appear on the search page
      'div#dismissible.style-scope.ytd-video-renderer', // Videos that appear on the search page
      'ytd-compact-video-renderer.style-scope.ytd-item-section-renderer', // Videos that appear on watch links
      'ytd-compact-radio-renderer.style-scope.ytd-item-section-renderer.use-ellipsis' // Videos that appear on watch links
    ]
    if (GM_getValue('console_log')) console.log("mixed video found", node);
    selectors.forEach(function(selector) {
      let nodeClosest = node.closest(selector);
      if (nodeClosest == null) { return; }
      nodeClosest.style.display = 'none';
      if (GM_getValue('console_log')) console.log("mixed video removed", nodeClosest);
    });
  }

  function played(node) {
    if (GM_getValue('played_visible')) { return; }
    if (!node.matches('div#progress.style-scope.ytd-thumbnail-overlay-resume-playback-renderer')) { return; }
    var selectors = [
      'ytd-rich-item-renderer.style-scope.ytd-rich-grid-row', // Vidoes that appear on the main page
      'ytd-video-renderer.style-scope.ytd-item-section-renderer', // Videos that appear on the search page
      'div#dismissible.style-scope.ytd-video-renderer', // Videos that appear on the search page
      'ytd-rich-item-renderer.style-scope.ytd-rich-grid-renderer',  // Videos that appear on watch links
      'ytd-compact-video-renderer.style-scope.ytd-item-section-renderer' // Videos that appear on watch links
    ]
    if (GM_getValue('console_log')) console.log("played video found", node);
    selectors.forEach(function(selector) {
      let nodeClosest = node.closest(selector);
      if (nodeClosest == null) { return; }
      nodeClosest.style.display = 'none';
      if (GM_getValue('console_log')) console.log("played video removed", nodeClosest);
    });
  }

  new window.MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      mutation.addedNodes.forEach(function(node) {
        // Only process element nodes
        if (node.nodeType != 1) return;

        // Autoplay
        autoplay(node);

        // Autoplay Visble
        autoplay_visible(node);

        // Mixes
        mixes(node);

        // Played
        played(node);
      });
    });
  }).observe(document, {
    childList: true,
    subtree: true,
    attributes: false,
    attributeOldValue: false,
    characterData: true,
    characterDataOldValue: false
  });
})();
