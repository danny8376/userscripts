// ==UserScript==
// @name        Twitter Tweaks
// @author      KonomiKitten
// @namespace   twittetweaks
// @description Twitter Tweaks
// @include     http://twitter.com*
// @include     https://twitter.com*
// @version     3.2.4
// @updateURL   https://gitlab.com/konomikitten/userscripts/-/raw/main/twitter-tweaks.user.js
// @downloadURL https://gitlab.com/konomikitten/userscripts/-/raw/main/twitter-tweaks.user.js
// @homepageURL https://gitlab.com/konomikitten/userscripts
// @supportURL  https://gitlab.com/konomikitten/userscripts/-/issues
// @icon        https://abs.twimg.com/favicons/favicon.ico
// @grant       none
// @run-at      document-start
// @inject-into content
// @noframes
// ==/UserScript==
